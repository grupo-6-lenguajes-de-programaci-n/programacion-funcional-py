import time

# Función para verificar si un número es primo
def es_primo(n):
    if n <= 1:
        return False
    for i in range(2, int(n ** 0.5) + 1):
        if n % i == 0:
            return False
    return True

# Rango de números a verificar
inicio = 1
fin = 50

# Medir tiempo de ejecución
start_time = time.time()

# Uso de función filter y expresión lambda para encontrar primos en el rango
numeros_primos = filter(lambda x: es_primo(x), range(inicio, fin + 1))

# Detener el cronómetro
end_time = time.time()

# Mostrar los números primos encontrados
print("Números primos en el rango de", inicio, "a", fin, ":", list(numeros_primos))
print("Tiempo de ejecución:", end_time - start_time, "segundos")